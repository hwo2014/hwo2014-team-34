package noobbot;

import java.util.ArrayList;
import java.util.List;

import noobbot.GameInit.Piece;
import noobbot.GameInit.RaceSession;
import noobbot.GameInit.Track;

import com.google.gson.Gson;

public class BotAi {

	Gson gson = new Gson();
	BotUtil botUtil = new BotUtil();
	double dist = 0;
	int piece = 0;
	Track track;
	boolean switchSend = false;
	int switchCounter = 0;
	int lap = 0;
	boolean justEnteredCurve = false;
	List<Curve> curves;
	final String name;
	CarPosition oldCar;
	boolean quickRace = true;
	boolean firstTimeIn0Piece = true;

	double totalDist = 0;
	int qcounter = 0;
	Integer laneTactics[];

	public BotAi(String name) {
		this.name = name;
	}

	void initTrack(MsgWrapper msg) {
		if (qcounter > 0) {
			quickRace = false;
		}
		String json = gson.toJson(msg.data);
		System.out.println(json);
		track = gson.fromJson(json, GameInit.class).race.track;

		System.out.println("quickRace:" + quickRace);
		if (quickRace == false) {

		} else {
			curves = initCurves(track);
			// curves = CurveFactory.getHockenHeimCurves3();
			laneTactics = botUtil.calculateSwitchTactics(track);
		}
		qcounter++;
	}

	private List<Curve> initCurves(final Track track) {
		int curveStartIdx = 0;
		int curveEndIdx = 0;
		List<Curve> curves = new ArrayList<Curve>();
		boolean firstCurve = true;
		for (int i = 0; i < track.pieces.length; i++) {
			Piece p = track.pieces[i];
			Piece prevP = track.pieces[(i - 1 + track.pieces.length)
					% track.pieces.length];
			if ((!p.isCurve() && (prevP.isCurve()))
					|| (prevP.isCurve() && p.isCurve() && prevP
							.isAnglePositive() != p.isAnglePositive())) {
				if (!firstCurve) {
					curveEndIdx = i - 1;
					curves.add(new TrackCurve(curveStartIdx, curveEndIdx, track));
				}
			}
			if (p.isCurve()
					&& ((prevP.isAnglePositive() != p.isAnglePositive() || !prevP
							.isCurve()))) {
				curveStartIdx = i;
				firstCurve = false;
			}

		}

		return curves;

	}

	public void carCrashed() {
		if (track.pieces[oldCar.piecePosition.pieceIndex].isCurve()) {
			getNextCurve(oldCar).crash();
		} else {
			getPreviousCurve(oldCar).crash();
		}
	}

	private double calculateDistToNextCar(CarPosition[] carPositions) {
		CarPosition myCar = getMyCar(carPositions);
		int myIdx = myCar.piecePosition.pieceIndex;
		int myLane = myCar.piecePosition.lane.endLaneIndex;
		double minDist = 10000;

		for (CarPosition carPosition : carPositions) {
			double distance = 0;
			double lane = carPosition.piecePosition.lane.endLaneIndex;
			if (carPosition.id.name.equals(name)) {
				continue;
			} else if (myLane != lane) {
				continue;
			} else {
				int pieceIdxCounter = myIdx;
				while (pieceIdxCounter != myIdx) {
					double laneDifference = track.lanes[myLane].distanceFromCenter;
					distance = distance
							+ track.pieces[pieceIdxCounter]
									.getLength(laneDifference);
				}
				distance = distance + carPosition.piecePosition.inPieceDistance
						- myCar.piecePosition.inPieceDistance;
			}
			if (distance < minDist) {
				minDist = distance;
			}
		}
		return minDist;
	}

	SendMsg calculateMessage(MsgWrapper msg) {
		double throttle;
		String json = gson.toJson(msg.data);
		CarPosition carPositions[] = gson.fromJson(json, CarPosition[].class);
		CarPosition myCar = getMyCar(carPositions);
		oldCar = myCar;
		double velocity = calculateVelocity(myCar);
		SendMsg sendMsg;
		checkLap(myCar);
		int currentPieceIdx = myCar.piecePosition.pieceIndex;
		Piece currentPiece = track.pieces[currentPieceIdx];
		Piece nextPiece = track.pieces[currentPieceIdx == (track.pieces.length - 1) ? 0
				: currentPieceIdx + 1];
		if (myCar.piecePosition.pieceIndex == 0) {
			if (firstTimeIn0Piece) {
				firstTimeIn0Piece = false;
			}
			switchCounter = 0;
		}
		if ((sendMsg = applySwitchTactics(myCar, nextPiece)) == null) {

			if (nextPiece.isSwitch == false) {
				switchSend = false;
			}
			if (currentPiece.isCurve()) {
				throttle = curveTactics(myCar, velocity);
			} else {
				throttle = straightTactics(myCar, velocity);
			}
			// if(lap == 1){
			// throttle = 0;
			// }
			// throttle = 1;
			sendMsg = new Throttle(throttle);
			// System.out.println(velocity + " " + myCar.angle + " "
			// + myCar.piecePosition.pieceIndex + " " + throttle + " " +
			// myCar.piecePosition.inPieceDistance + " " +
			// distanceToNextCorner(myCar));
			// System.out.println(velocity + " " + totalDist);
		}
		return sendMsg;
	}

	private CarPosition getMyCar(CarPosition[] carPositions) {
		for (int i = 0; i < carPositions.length; i++) {
			if (carPositions[i].id.name.equals(name)) {
				return carPositions[i];
			}
		}
		return null;
	}

	private double straightTactics(CarPosition myCar, double velocity) {
		double throttle = 0;
		double targetVelocity = getNextCurve(myCar)
				.getTargetVelocity(quickRace);
		double distanceRequired = botUtil.calculateBreakDist(velocity)
				- botUtil.calculateBreakDist(targetVelocity);
		if (distanceToNextCorner(myCar) > (distanceRequired + velocity * 2)) {
			throttle = 1;
		} else {
			throttle = 0;
		}

		return throttle;
	}

	private double curveTactics(CarPosition myCar, double velocity) {

		boolean jec;

		if (justEnteredCurve) {
			Curve currentCurve = getNextCurve(myCar);
			currentCurve.setIncomingAngle(myCar.angle);
			Curve c = getPreviousCurve(myCar);
			int curveIdx = curves.indexOf(c);
			if (lap == 0 && curveIdx == curves.size() - 1) {
				// dont apply the first curve of first lap
			} else {
				if (quickRace) {
					c.updateTargetVelocity();
					System.out.println("ov: " + c.getOldTargetVelocity()
							+ " nv: " + c.getTargetVelocity(quickRace)
							+ " curve: " + curves.indexOf(c) + " a: "
							+ c.getMaxAngle() + " in: " + c.getIncomingAngle()
							+ " csi: " + c.getCurveStartIdx());
				}

			}

			this.justEnteredCurve = false;

		}
		double throttle = 0;
		Curve currentCurve = getNextCurve(myCar);
		currentCurve.updateMaxAngle(myCar.angle);
		double targetVelocity = currentCurve.getTargetVelocity(quickRace);
		if (velocity > targetVelocity) {
			throttle = 0;
		} else {
			throttle = 1;
		}
		// System.out.println(curves.indexOf(currentCurve));

		Curve nextCurve = curves.get((curves.indexOf(currentCurve) + 1)
				% curves.size());
		int nextCurveIdx = curves.indexOf(nextCurve);

		double nextTargetVelocity = nextCurve.getTargetVelocity(quickRace);
		double distanceRequired = botUtil.calculateBreakDist(velocity)
				- botUtil.calculateBreakDist(nextTargetVelocity);
		// System.out.println("cci: " +curves.indexOf(currentCurve) + " nci: " +
		// nextCurveIdx + " ntv: " + nextTargetVelocity + " dr: "
		// +distanceRequired);
		if (distanceToNextCorner(myCar) > (distanceRequired + velocity * 2)) {

		} else {
			throttle = 0;
		}

		return throttle;
	}

	// return the next or the current curve
	private Curve getNextCurve(CarPosition myCar) {
		int currentPieceIdx = myCar.piecePosition.pieceIndex;
		for (Curve c : curves) {
			if (currentPieceIdx <= c.getCurveEndIdx()) {
				return c;
			}
		}
		return curves.get(0);
	}

	private Curve getPreviousCurve(CarPosition myCar) {
		int curveIdx = curves.indexOf(getNextCurve(myCar));
		int previousCurveIdx = curveIdx - 1;
		if (previousCurveIdx < 0) {
			previousCurveIdx = curves.size() - 1;
		}
		return curves.get(previousCurveIdx);
	}

	private SendMsg applySwitchTactics(CarPosition myCar, Piece nextPiece) {
		if (nextPiece.isSwitch && !switchSend) {
			if (switchCounter >= laneTactics.length) {
				switchCounter = 0;
			}
			switchSend = true;
			if (myCar.piecePosition.lane.endLaneIndex > laneTactics[switchCounter]) {
				switchCounter++;
				return new SwitchLane("Left");

			} else if (myCar.piecePosition.lane.endLaneIndex < laneTactics[switchCounter]) {
				switchCounter++;
				return new SwitchLane("Right");
			} else {
				switchCounter++;
			}
		}
		return null;
	}

	private void checkLap(CarPosition myCar) {
		if (myCar.piecePosition.lap > lap) {
			lap = myCar.piecePosition.lap;
			switchCounter = 0;
		}
	}

	private double calculateVelocity(CarPosition myCar) {

		double currentDist = myCar.piecePosition.inPieceDistance;
		int currentPiece = myCar.piecePosition.pieceIndex;
		double velocity;

		if (currentPiece != piece) {
			double distFromCenter = getDistFromCenter(myCar);
			double prevPieceLen = track.pieces[piece].getLength(distFromCenter);
			velocity = prevPieceLen + currentDist - dist;
			if (track.pieces[currentPiece].isCurve())
				if (!track.pieces[piece].isCurve()
						|| track.pieces[piece].isAnglePositive() != track.pieces[currentPiece]
								.isAnglePositive()) {
					this.justEnteredCurve = true;
				}
		} else {
			velocity = currentDist - dist;
		}
		totalDist = totalDist + velocity;
		dist = currentDist;
		piece = currentPiece;
		return velocity;
	}

	private double getDistFromCenter(CarPosition myCar) {
		int laneIdx = myCar.piecePosition.lane.startLaneIndex;
		double distFromCenter = track.lanes[laneIdx].distanceFromCenter;
		return distFromCenter;
	}

	private double distanceToNextCorner(CarPosition myCar) {
		int currentPiece = myCar.piecePosition.pieceIndex;
		double distance = track.pieces[currentPiece]
				.getLength(getDistFromCenter(myCar)) - dist;
		int i = (currentPiece + 1) % track.pieces.length;
		while (true) {
			if ((track.pieces[i].isCurve() && !track.pieces[currentPiece]
					.isCurve())
					|| (track.pieces[currentPiece].isAnglePositive() != track.pieces[i]
							.isAnglePositive())) {
				return distance;
			} else {
				distance += track.pieces[i].getLength(getDistFromCenter(myCar));
			}
			i++;
			i = i % track.pieces.length;
		}
	}

}
