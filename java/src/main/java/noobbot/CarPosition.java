package noobbot;

class CarPosition {
	Id id;
	double angle;
	PiecePosition piecePosition;

	class Id {
		String name;
		String color;
	}

	class PiecePosition {
		int pieceIndex;
		double inPieceDistance;
		Lane lane;
		int lap;
	}

	class Lane {
		int startLaneIndex;
		int endLaneIndex;
	}

}