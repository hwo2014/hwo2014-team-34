package noobbot;

public interface Curve {

	void crash();
	int getCurveEndIdx();
	int getCurveStartIdx();
	double getMaxAngle();
	double getOldTargetVelocity();
	double getTargetVelocity(boolean quickRace);
	void updateMaxAngle(double angle);
	void updateTargetVelocity();
	public double getIncomingAngle() ;
	public void setIncomingAngle(double incomingAngle);
	
}