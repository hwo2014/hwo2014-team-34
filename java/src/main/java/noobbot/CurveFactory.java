package noobbot;

import java.util.Arrays;
import java.util.List;

public class CurveFactory {
	
	static List<Curve> getHockenHeimCurves(){
		Curve c0 = new MockCurve(4.940110323089047, 2 ,4);
		Curve c1 = new MockCurve(4.5401103230890465, 6, 8);
		Curve c2 = new MockCurve(4.232505910165486, 10, 13);
		Curve c3 = new MockCurve(6.6, 16, 16);
		Curve c4 = new MockCurve(5.562508691419831, 18, 20);
		Curve c5 = new MockCurve(4.355082742316785, 23, 26);
		Curve c6 = new MockCurve(5.3500000000000005, 28, 29);
		Curve c7 = new MockCurve(6.0, 31, 31);
		Curve c8 = new MockCurve(7.0, 34, 35);
		Curve c9 = new MockCurve(5.262508691419831, 37, 39);
		Curve c10 = new MockCurve(6.6, 43, 43);
		Curve c11 = new MockCurve(5.178132387706857, 46, 49);
		Curve c12 = new MockCurve(6.300000000000001, 51, 53);
		return Arrays.asList(c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12);

	}

	static List<Curve> getHockenHeimCurves2(){
		Curve c0 = new MockCurve(5.2175110323089047, 2 ,4);
		Curve c1 = new MockCurve(4.81103230890465, 6, 8);
		Curve c2 = new MockCurve(4.7232505910165486, 10, 13);
		Curve c3 = new MockCurve(7.8, 16, 16);
		Curve c4 = new MockCurve(5.792508691419831, 18, 20);
		Curve c5 = new MockCurve(4.485082742316785, 23, 26);
		Curve c6 = new MockCurve(5.420000000000005, 28, 29);
		Curve c7 = new MockCurve(7.0, 31, 31);
		Curve c8 = new MockCurve(7.8, 34, 35);
		Curve c9 = new MockCurve(5.462508691419831, 37, 39);
		Curve c10 = new MockCurve(7.7, 43, 43);
		Curve c11 = new MockCurve(5.90828132387706857, 46, 49);
		Curve c12 = new MockCurve(6.650000000000001, 51, 53);
		return Arrays.asList(c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12);

	}
	
	static List<Curve> getHockenHeimCurves3(){
		Curve c0 = new MockCurve(4.81522633744856, 2 ,4);
		Curve c1 = new MockCurve(5.460226337448561, 6, 8);
		Curve c2 = new MockCurve(5.675617283950618, 10, 13);
		Curve c3 = new MockCurve(6.75, 16, 16);
		Curve c4 = new MockCurve(6.3549999999999995, 18, 20);
		Curve c5 = new MockCurve(5.386419753086420, 23, 26);
		Curve c6 = new MockCurve(5.75, 28, 29);
		Curve c7 = new MockCurve(6.05, 31, 31);
		Curve c8 = new MockCurve(8.292500000000000, 34, 35);
		Curve c9 = new MockCurve(5.631, 37, 39);
		Curve c10 = new MockCurve(8.55, 43, 43);
		Curve c11 = new MockCurve(5.33271604938272, 46, 49);
		Curve c12 = new MockCurve(7.40000000000001, 51, 53);
		return Arrays.asList(c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12);

	}
	
	static List<Curve> getFranceCurves(){
		Curve c0 = new MockCurve(4.81522633744856, 2 ,7);
		Curve c1 = new MockCurve(5.460226337448561, 6, 8);
		Curve c2 = new MockCurve(5.675617283950618, 10, 13);
		Curve c3 = new MockCurve(6.75, 16, 16);
		Curve c4 = new MockCurve(6.3549999999999995, 18, 20);
		Curve c5 = new MockCurve(5.386419753086420, 23, 26);
		Curve c6 = new MockCurve(5.75, 28, 29);
		Curve c7 = new MockCurve(6.05, 31, 31);
		Curve c8 = new MockCurve(8.292500000000000, 34, 35);
		Curve c9 = new MockCurve(5.631, 37, 39);
		Curve c10 = new MockCurve(8.55, 43, 43);
		Curve c11 = new MockCurve(5.33271604938272, 46, 49);
		Curve c12 = new MockCurve(7.40000000000001, 51, 53);
		return Arrays.asList(c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12);

	}
	
	
}