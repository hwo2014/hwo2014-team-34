package noobbot;

import com.google.gson.annotations.SerializedName;

class GameInit {
	Race race;

	class Race {
		Track track;
		RaceSession raceSession;
	}
	
	class RaceSession {
		boolean quickRace;
	}

	class Track {
		String id;
		String name;
		Piece[] pieces;
		Lane[] lanes;
		StartingPoint startingPoint;
	}

	class Piece {
		private double length;
		double radius;
		double angle;
		@SerializedName("switch")
		boolean isSwitch;

		public boolean isCurve() {
			return angle != 0;
		}
		
		public boolean isAnglePositive() {
			return angle > 0;
		}

		double getLength(double laneDifference) {
			if (this.isCurve()) {
				double laneRadius = (angle < 0) ? radius + laneDifference : radius - laneDifference;
				return Math.abs(angle) / 360 * 2 * laneRadius * Math.PI;
			}
			return length;
		}
	}

	class Lane {
		double distanceFromCenter;
		int index;
	}

	class StartingPoint {
		Position position;
		double angle;
	}

	class Position {
		double x;
		double y;
	}
}