package noobbot;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.google.gson.Gson;

public class Logger {

	final Gson gson = new Gson();
	Writer writer;
	final String filename;
	
	public Logger(){
		DateFormat dateFormat = new SimpleDateFormat("MM_dd_HH_mm_ss");
		Calendar cal = Calendar.getInstance();
		filename = dateFormat.format(cal.getTime()) + ".log";

	}

	void log(String loggable) throws IOException {
		if (Config.LOGGING_ENABLED) {
			System.out.println(loggable);
		/*	writer = new FileWriter(Config.LOG_DIR + filename, true);
			writer.write(loggable + '\n');
			writer.close();*/
		}
	}
	
	void logSend(String msg) throws IOException{
		log("{\"sent\" : " + msg + "},");
	}
	
	void logReceived(String msg) throws IOException{
		log("{\"received\" : " + msg + "},");
	}

}
