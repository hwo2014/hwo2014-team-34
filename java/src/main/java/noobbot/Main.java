package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

public class Main {

	Logger logger = new Logger();

	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];
		SendMsg sendMsg = null;
		String trackName = "keimola";
		String racePassword = "";

		if (args.length == 4) {
			sendMsg = new Join(botName, botKey);
		} else if (args.length == 6) {
			String msgType = args[4];
			racePassword = args[5];
			sendMsg = new RaceMsg(botName, botKey, trackName, racePassword,
					msgType, 1);
		} else {
			System.out.println("Invalid number of params");
		}

		System.out.println("Connecting to " + host + ":" + port + " as "
				+ botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(
				socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(
				socket.getInputStream(), "utf-8"));

		new Main(reader, writer, sendMsg, botName);
	}

	final Gson gson = new Gson();
	private PrintWriter writer;

	public Main(final BufferedReader reader, final PrintWriter writer,
			final SendMsg join, String name) {
		try {
			this.writer = writer;
			String line = null;
			send(join);
			BotAi ai = new BotAi(name);
			while ((line = reader.readLine()) != null) {
				final MsgWrapper msgFromServer = gson.fromJson(line,
						MsgWrapper.class);
				logger.logReceived(gson.toJson(msgFromServer).toString());
				if (msgFromServer.msgType.equals("carPositions")) {
					send(ai.calculateMessage(msgFromServer));
				} else if (msgFromServer.msgType.equals("join")) {
					System.out.println("Joined");
					send(new Ping());
				} else if (msgFromServer.msgType.equals("gameInit")) {
					ai.initTrack(msgFromServer);
					System.out.println("Race init");
					send(new Ping());
				} else if (msgFromServer.msgType.equals("gameEnd")) {
					System.out.println("Race end");
					send(new Ping());
				} else if (msgFromServer.msgType.equals("gameStart")) {
					System.out.println("Race start");
					send(new Ping());
				} else if (msgFromServer.msgType.equals("joinRace")) {
					System.out.println("Joining race");
					send(new Ping());
				} else if (msgFromServer.msgType.equals("createRace")) {
					System.out.println("Creating race");
					send(new Ping());
				} else if (msgFromServer.msgType.equals("lapFinished")) {
					System.out.println("lap finished");
					send(new Ping());
				} else if (msgFromServer.msgType.equals("crash")) {
					
					String json = gson.toJson(msgFromServer.data);
					CrashInfo crashInfo = gson.fromJson(json, CrashInfo.class);
					if(crashInfo.name.equals(name)){
						System.out.println("crashed");
						ai.carCrashed();
					}
					send(new Ping());
				} else if (msgFromServer.msgType.equals("spawn")) {
					System.out.println("spawned");
					send(new Ping());
				} else {
					send(new Ping());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void send(final SendMsg msg) throws IOException {
		logger.logSend(msg.toJson().toString());
		writer.println(msg.toJson());
		writer.flush();
	}
}

abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
}

class MsgWrapper {
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}
}

class CrashInfo {
	String name;
	String color;
}

class RaceMsg extends SendMsg {

	public final BotId botId;
	// public final String password;
	public final String trackName;
	public final String msgType;
	public final int carCount;

	public RaceMsg(final String name, final String key, final String trackname,
			final String password, final String msgType, final int carCount) {
		this.botId = new BotId(name, key);
		this.trackName = trackname;
		// this.password = password;
		this.carCount = carCount;
		this.msgType = msgType;
	}

	@Override
	protected String msgType() {
		return msgType;
	}

	class BotId {
		final String name;
		final String key;

		public BotId(final String name, final String key) {
			this.name = name;
			this.key = key;
		}
	}

}

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}

class SwitchLane extends SendMsg {

	private String direction;

	public SwitchLane(String direction) {
		this.direction = direction;
	}

	@Override
	protected Object msgData() {
		return direction;
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}

}

class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}