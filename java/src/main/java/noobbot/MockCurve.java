package noobbot;

public class MockCurve implements Curve {

	final double mockVelocity;
	final int curveStartIdx;
	final int curveEndIdx;
	private double maxAngle =0;

	public MockCurve(double mockVelocity, int curveStartIdx, int curveEndIdx) {
		this.mockVelocity = mockVelocity;
		this.curveStartIdx = curveStartIdx;
		this.curveEndIdx = curveEndIdx;
	}

	@Override
	public void crash() {
		// TODO Auto-generated method stub

	}

	@Override
	public int getCurveEndIdx() {
		return curveEndIdx;
	}

	@Override
	public int getCurveStartIdx() {
		return curveStartIdx;
	}

	@Override
	public double getMaxAngle() {
		return maxAngle;
	}



	@Override
	public void updateMaxAngle(double angle) {
		this.maxAngle = angle;

	}

	@Override
	public void updateTargetVelocity() {
		// TODO Auto-generated method stub

	}

	@Override
	public double getOldTargetVelocity() {
		// TODO Auto-generated method stub
		return mockVelocity;
	}

	@Override
	public double getIncomingAngle() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setIncomingAngle(double incomingAngle) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double getTargetVelocity(boolean quickRace) {
		// TODO Auto-generated method stub
		return 0;
	}

}