package noobbot;

import noobbot.GameInit.Piece;
import noobbot.GameInit.Track;

//A curve can consist of multuple pieces!
public class TrackCurve implements Curve {

	private final int curveStartIdx;
	private final int curveEndIdx;
	private final Track track;

	private int nOfeasyC = 0;
	private int nOfmediumC = 0;
	private int nOfhardC = 0;

	// the angle the the car had when entering the curve
	private double incomingAngle = 0;

	// the maximum angle the car had with previous velocity
	private double maxAngle = 0;

	// the minimum radius the curve has(in a single piece)
	private double minRadius = 1000;

	// whether the car has crashed to this curve during the race
	private boolean crashed = false;

	private double targetVelocity = 0;
	private double oldTargetVelocity = 0;

	public TrackCurve(int curveStartIdx, int curveEndIdx, Track track) {
		this.curveStartIdx = curveStartIdx;
		this.curveEndIdx = curveEndIdx;
		this.track = track;
		targetVelocity = estimateStartVelocity();
	}

	// estimate a velocity
	private double estimateStartVelocity() {
		double totalDifficulty = 0;
		for (int i = this.curveStartIdx; track.pieces[i].isCurve(); i++) {

			Piece p = track.pieces[i];

			if (p.angle < 51) {
				nOfhardC++;
			} else if (p.angle < 101) {
				nOfmediumC++;
			} else if (p.angle < 201) {
				nOfeasyC++;
			}

			double pieceDifficulty = Math.abs(p.angle) / Math.pow(p.radius, 2);
			totalDifficulty = totalDifficulty + pieceDifficulty;
		}
		double startVel = (1 / (4.5 * totalDifficulty));
		return Math.min(startVel, 5.5);
	}

	public void crash() {
		crashed = true;
		targetVelocity = targetVelocity - 0.4;
	}

	public double getMaxAngle() {
		return maxAngle;
	}

	public void updateMaxAngle(double angle) {
		if (Math.abs(angle) > maxAngle) {
			this.maxAngle = Math.abs(angle);
		}
	}

	public int getCurveStartIdx() {
		return curveStartIdx;
	}

	public int getCurveEndIdx() {
		return curveEndIdx;
	}

	public double getOldTargetVelocity() {
		return Math.min(oldTargetVelocity, 10);
	}

	public double getTargetVelocity(boolean quickRace) {
		if(quickRace){
		return Math.min(targetVelocity, 10);
		}
		return getOldTargetVelocity();
	}

	public void updateTargetVelocity() {

		oldTargetVelocity = targetVelocity;
		double modAngle = this.maxAngle;
		modAngle = modAngle + Math.abs(incomingAngle);
		if (!this.crashed) {
			double factor = 2.5;
			if (nOfhardC > 1) {
				factor = factor * 0.4;
			}
			if (modAngle < 10) {
				this.targetVelocity = this.targetVelocity + 0.6 * factor;

			} else if (modAngle < 20) {
				this.targetVelocity = this.targetVelocity + 0.35 * factor;

			} else if (modAngle < 30) {
				this.targetVelocity = this.targetVelocity + 0.27 * factor;

			} else if (modAngle < 40) {
				this.targetVelocity = this.targetVelocity + 0.15 * factor;
			}
		}
	}

	public double getIncomingAngle() {
		return incomingAngle;
	}

	public void setIncomingAngle(double incomingAngle) {
		this.incomingAngle = incomingAngle;
	}
}
